import os
import subprocess
import git
import requests

def clone_repository(repo_url, clone_dir):
    try:
        print(f"Cloning repository from {repo_url} into {clone_dir}...")
        git.Repo.clone_from(repo_url, clone_dir)
        print("Repository cloned successfully.")
    except Exception as e:
        print(f"Failed to clone repository: {e}")

def download_and_replace_file(url, destination):
    try:
        print(f"Downloading file from {url} to {destination}...")
        response = requests.get(url)
        with open(destination, 'wb') as f:
            f.write(response.content)
        print("File downloaded and replaced successfully.")
    except Exception as e:
        print(f"Failed to download and replace file: {e}")

def install_package(clone_dir):
    try:
        print(f"Installing package from {clone_dir}...")
        os.chdir(clone_dir)
        subprocess.run(['pip', 'install', '-e', '.'])
        print("Package installed successfully.")
    except Exception as e:
        print(f"Failed to install package: {e}")

def create_data_folder():
    try:
        print("Creating data folder...")
        os.makedirs('data', exist_ok=True)
        print("Data folder created successfully.")
    except Exception as e:
        print(f"Failed to create data folder: {e}")

def set_env_variable(key, value):
    try:
        print(f"Setting environment variable {key}...")
        os.environ[key] = value
        print(f"Environment variable {key} set successfully.")
    except Exception as e:
        print(f"Failed to set environment variable: {e}")

def run_batch_script(script_path):
    try:
        print(f"Running batch script {script_path}...")
        env = os.environ.copy()
        env['CMPATH'] = os.path.expanduser("~/CellModeller")
        script_args = script_path.split(" ")
        script_args = [os.path.expanduser(arg) for arg in script_args]
        subprocess.run(['python', *script_args], env=env)
        print("Batch script run successfully.")
    except Exception as e:
        print(f"Failed to run batch script: {e}")

if __name__ == '__main__':
    try:
        repo_url = "https://github.com/cellmodeller/CellModeller.git"
        clone_dir = "CellModeller"
        custom_batch_url = "https://gitlab.com/-/snippets/3603290/raw/main/batch.py"
        custom_setup_url = "https://gitlab.com/-/snippets/3603290/raw/main/setup.py"
        custom_clbacterium_url = "https://gitlab.com/-/snippets/3603290/raw/main/CLBacterium.py"
        custom_cellmodeller_test_url = "https://gitlab.com/-/snippets/3603290/raw/main/Tutorial_2a.py"

        # Step 1: Clone the repository
        clone_repository(repo_url, clone_dir)

        # Step 2: Download and replace custom batch.py and setup.py
        download_and_replace_file(custom_batch_url, os.path.join(clone_dir, "Scripts", "batch.py"))
        download_and_replace_file(custom_setup_url, os.path.join(clone_dir, "setup.py"))
        download_and_replace_file(custom_clbacterium_url, os.path.join(clone_dir, "CellModeller", "Biophysics", "BacterialModels", "CLBacterium.py"))
        download_and_replace_file(custom_cellmodeller_test_url, os.path.join(clone_dir, "Examples", "Tutorial_2", "Tutorial_2a.py"))


        # Step 3: Install the package in editable mode
        install_package(clone_dir)

        # Step 4: Create a data folder
        create_data_folder()

        # Step 5: Set CMPATH environment variable
        cm_path = os.path.expanduser("~/CellModeller")
        set_env_variable("CMPATH", cm_path)

        # Step 6: Run the batch script
        script_path = "~/CellModeller/Scripts/batch.py ~/CellModeller/Examples/Tutorial_2/Tutorial_2a.py"
        run_batch_script(script_path)
        
        print("All operations completed successfully.")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")
