import horovod.torch as hvd
import torch
import torch.nn as nn
import torch.optim as optim
import netifaces as ni
import numpy as np
import time
import os
import socket
import threading

# Set the default data type to float32
torch.set_default_dtype(torch.float32)

def get_job_initiator_node_ip():
    # Define the possible interface names in the desired order of preference
    #interface_names = ['eth0', 'eth1', 'enp3s0', 'enp4s0f0', 'eth2', 'eth3', 'ens0', 'ens1', 'p1p1', 'p2p1']
    interface_names = ['hv0', 'hv1', 'hv2', 'hv3', 'hv4', 'hv5', 'hv6', 'hv7', 'hv8', 'hv9']
    
    # Iterate over the interface names and select the first available IP address
    job_initiator_node_interface = None
    job_initiator_node_ip = None

    for interface_name in interface_names:
        if interface_name in ni.interfaces():
            try:
                # Get the IP address of the interface
                ip_address = ni.ifaddresses(interface_name)[ni.AF_INET][0]['addr']

                # If the IP address exists, set the job_initiator_node_interface and job_initiator_node_ip
                if ip_address:
                    job_initiator_node_interface = interface_name
                    job_initiator_node_ip = ip_address
                    break
            except KeyError:
                pass

    # If no IP address is found, fallback to the default IP address (127.0.0.1)
    if not job_initiator_node_interface or not job_initiator_node_ip:
        job_initiator_node_ip = socket.gethostbyname(socket.gethostname())
        job_initiator_node_interface = 'default'

    # Print the job_initiator node IP address
    print(f"Selected job_initiator node IP address: {job_initiator_node_ip} (Interface: {job_initiator_node_interface})")

    return job_initiator_node_ip, job_initiator_node_interface

# Initialize Horovod
hvd.init()

# Set a seed for reproducibility
torch.manual_seed(0)

# Horovod assigns each worker a unique rank
rank = hvd.rank()

# Get the IP and port of a job_initiator node (this could be one of your containers)
# job_initiator_node_ip = socket.gethostbyname(socket.getfqdn())  # Replace with the IP of your job_initiator node
# Call the function to retrieve the job_initiator node IP address
if os.getenv("WORKER_INDEX") == "1":
    job_initiator_node_ip, job_initiator_node_interface = get_job_initiator_node_ip()
    os.environ["JOB_INITIATOR_NODE_IP"] = job_initiator_node_ip
else:
    job_initiator_node_ip = os.getenv("JOB_INITIATOR_NODE_IP")

job_initiator_node_start_port = 12345  # Replace with the port of your job_initiator node for start synchronization
job_initiator_node_end_port = 12346  # Replace with the port of your job_initiator node for end synchronization
num_workers = 5  # Replace with the number of workers

# Load the entire dataset
def load_data():
    # Replace with your actual code to load the data
    data = np.random.rand(1000000, num_features).astype(np.float32), np.random.rand(1000000).astype(np.float32)
    # Shuffle data
    indices = np.arange(data[0].shape[0])
    np.random.shuffle(indices)
    return data[0][indices], data[1][indices]

def server(port, expected_msg, response_msg):
    # Set up a server socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((job_initiator_node_ip, port))  # use the specific port passed as argument
    s.listen(num_workers)

    # Keep track of the number of ready messages received
    num_received = 0

    # Store the client sockets to send them the response message later
    clientsockets = []

    while True:
        # Accept a connection
        clientsocket, address = s.accept()
        clientsockets.append(clientsocket)
        print(f"Connection from {address} has been established.")

        # Receive a message
        data = clientsocket.recv(1024)

        # If the message is the expected_msg, increment the count of received messages
        if data == expected_msg:
            num_received += 1
            print(f"Received {expected_msg.decode('utf-8')} message from {address}. Total {expected_msg.decode('utf-8')}: {num_received}")

        # If all containers are ready, send a response_msg message to each container and break the loop
        if num_received == num_workers:
            print(f"All workers are {expected_msg.decode('utf-8')}. Sending {response_msg.decode('utf-8')} message...")
            for cs in clientsockets:
                cs.send(response_msg)
            break

    # Close the server socket
    s.close()


        

# If this is the first worker, start the server in a separate thread for start and end synchronizations
if os.getenv("WORKER_INDEX") == "1":
    threading.Thread(target=server, args=(job_initiator_node_start_port, b"ready", b"start")).start()  # start synchronization
    threading.Thread(target=server, args=(job_initiator_node_end_port, b"done", b"end")).start()  # end synchronization

# Create a socket and connect to the job_initiator node
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect((job_initiator_node_ip, job_initiator_node_start_port))
        break
    except socket.error as e:
        #print(f"Failed to connect: {e.strerror}. Retrying in 1 second...")
        print(f"Failed to connect to job initiator: {e.strerror}. Retrying...")
        time.sleep(1)

if os.getenv("WORKER_INDEX") != "1":
    print(f"Container {os.getenv('WORKER_INDEX')} connected to job initiator. Launching job after full synchronization...")

# Send a message to the job_initiator node to indicate that this container is ready
s.sendall(b"ready")

# Receive a message from the job_initiator node. This will block until the job_initiator node sends a message.
data = s.recv(1024)

# Close the socket
s.close()

# Check the message. If it's "start", all containers are ready and this container can start.
if data != b"start":
    print(f"Received unexpected message: {data.decode('utf-8')}. Exiting...")
    exit(1)

# Define the number of features and the learning rate
num_features = 10
learning_rate = 0.001

# Horovod: pin GPU to be used to process local rank (one GPU per process)
if torch.cuda.is_available():
    torch.cuda.set_device(hvd.local_rank())
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# Print a message to indicate that the container has started
print(f"Container {os.getenv('WORKER_INDEX')} started.")

# Scale the learning rate by the number of workers
learning_rate *= hvd.size()

# Define PyTorch model (for example, a simple linear regression)
class Model(nn.Module):
    def __init__(self, num_features):
        super(Model, self).__init__()
        self.weight = nn.Parameter(torch.zeros(num_features, 1), requires_grad=True)
        self.bias = nn.Parameter(torch.zeros(1), requires_grad=True)

    def forward(self, x):
        return torch.matmul(x, self.weight) + self.bias

# Instantiate the model and move it to the device (e.g., GPU)
num_features = 10
model = Model(num_features).to(device)

# Define loss
loss_fn = nn.MSELoss()

# Define the optimizer
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# Wrap the optimizer with Horovod's DistributedOptimizer
optimizer = hvd.DistributedOptimizer(optimizer)

# Broadcast initial variable values from the first worker to all workers
hvd.broadcast_parameters(model.state_dict(), root_rank=0)

inputs, targets = load_data() # Adapt this function for PyTorch

inputs = torch.tensor(inputs).to(device)
targets = torch.tensor(targets).to(device)

# Compute the size of a chunk
chunk_size = len(inputs) // num_workers

# Compute the start and end indices for the chunk of this worker
start_index = rank * chunk_size
end_index = start_index + chunk_size if os.getenv("WORKER_INDEX") != num_workers - 1 else len(inputs)

# Extract the chunk for this worker
inputs = inputs[start_index:end_index]
targets = targets[start_index:end_index]

# Training loop
num_epochs = 200

# Perform the training loop
for epoch in range(num_epochs):
    # Forward pass
    outputs = model(inputs)
    outputs = outputs.squeeze()  # Remove any unnecessary dimensions
    loss_value = loss_fn(outputs, targets)

    # Backward pass and optimization
    optimizer.zero_grad()
    loss_value.backward()
    optimizer.step()

    print(f"Epoch {epoch+1}/{num_epochs}, Loss: {loss_value.item()}")

print("Waiting for remaining workers to finish...")
# hvd.allreduce(tf.constant(0))
# Add a barrier here to ensure all workers have finished training
# hvd.join()

# Create a socket and connect to the job_initiator node
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect((job_initiator_node_ip, job_initiator_node_end_port))  # use port 12346 for end synchronization
        break
    except socket.error as e:
        #print(f"Failed to connect: {e.strerror}. Retrying in 1 second...")
        print(f"Failed to connect to job initiator: {e.strerror}. Retrying...")
        
        time.sleep(1)

# Send a message to the job_initiator node to indicate that this worker is done
s.sendall(b"done")

# Receive a message from the job_initiator node. This will block until the job_initiator node sends a message.
data = s.recv(1024)

# Close the socket
s.close()

# Check the message. If it's "end", all workers are done and this worker can end.
if data != b"end":
    print(f"Received unexpected message: {data.decode('utf-8')}. Exiting...")
    exit(1)

# Average the model parameters across all workers
for name, param in model.named_parameters():
    avg_param = hvd.allreduce(param.data, name=name)
    param.data = avg_param

print("Model weights and biases synchronized across all workers.")

# Gather the weights and biases from the model's linear layer
weights = model.weight.data
biases = model.bias.data

# Compute the mean across all dimensions
average_weights = torch.mean(weights)
average_biases = torch.mean(biases)

print("Training finished.")
print(f"Average weights: {average_weights}")
print(f"Average biases: {average_biases}")

# To Test a Demo:

# Machine 1 with 4 NVIDIA GPUs

# docker run -ti --gpus device=0 --privileged --network=host -e JOB_INITIATOR_NODE_IP=10.99.99.1 -e WORKER_INDEX=1 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv0 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/develop/examples/horovod/horovod-pt-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container1 nvidia_horovod

# docker run -ti --gpus device=1 --privileged --network=host -e JOB_INITIATOR_NODE_IP=10.99.99.1 -e WORKER_INDEX=2 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv1 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/develop/examples/horovod/horovod-pt-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container2 nvidia_horovod

# docker run -ti --gpus device=2 --privileged --network=host -e JOB_INITIATOR_NODE_IP=10.99.99.1 -e WORKER_INDEX=3 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv2 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/develop/examples/horovod/horovod-pt-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container3 nvidia_horovod

# docker run -ti --gpus device=3 --privileged --network=host -e JOB_INITIATOR_NODE_IP=10.99.99.1 -e WORKER_INDEX=4 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv3 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/develop/examples/horovod/horovod-pt-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container4 nvidia_horovod

# Machine 2 with 1 NVIDIA GPU

# docker run -ti --gpus device=0 --privileged --network=host -e JOB_INITIATOR_NODE_IP=10.99.99.1 -e WORKER_INDEX=5 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv4 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/develop/examples/horovod/horovod-pt-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container5 nvidia_horovod