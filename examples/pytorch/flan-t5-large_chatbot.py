import gradio as gr
import torch
from transformers import T5Tokenizer, T5ForConditionalGeneration

# Load the pre-trained model and tokenizer
model_name = "google/flan-t5-large"
tokenizer = T5Tokenizer.from_pretrained(model_name)
model = T5ForConditionalGeneration.from_pretrained(model_name, return_dict=True).to("cuda:0")
#tokenizer = AutoTokenizer.from_pretrained(model_name)
#tokenizer.add_special_tokens({'pad_token': '[PAD]'})
#model = AutoModelForCausalLM.from_pretrained(model_name).to("cuda")

# Define a function to generate a response to user input
def chatbot(text):
    # Tokenize the input text and convert to a PyTorch tensor
    #input_ids = tokenizer.encode(text, return_tensors="pt", padding=True, truncation=True).to("cuda")
    input_ids = tokenizer(text, return_tensors="pt", padding=True).input_ids.to("cuda:0")
    # Generate a response using the model
    #output = model.generate(input_ids, max_length=1000, pad_token_id=tokenizer.eos_token_id, do_sample=True, top_p=0.9, temperature=1.0)
    outputs = model.generate(input_ids, min_length=256, max_new_tokens=1024, num_beams=16, length_penalty=1.4, no_repeat_ngram_size=2, temperature=0.7, top_k=150, top_p=0.92, repetition_penalty=2.1, early_stopping=True)
    #outputs = model.generate(input_ids, min_length=256, max_new_tokens=1024, num_beams=5, no_repeat_ngram_size=2, temperature=0.1, early_stopping=True)
    # Decode the response and return it
    response = tokenizer.decode(outputs[0], skip_special_tokens=True)
    #response = response.replace("<pad>", "").replace("</s>", "").replace("<unk>", "").replace("br>", "").strip()
    return response

# Create a Gradio interface
iface = gr.Interface(fn=chatbot, inputs="text", outputs="text", title="ChatBot",
                     description="Enter some text and the chatbot will generate a response.")
