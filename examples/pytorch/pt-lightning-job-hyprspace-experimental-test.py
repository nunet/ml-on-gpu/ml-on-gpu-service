import os
import socket
import threading
import pytorch_lightning as pl
from pytorch_lightning import Trainer
import torch
import netifaces as ni
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
import time

if os.getenv("WORKER_INDEX") == "1":
    os.environ['NCCL_SOCKET_IFNAME'] = 'hv0'

def get_job_initiator_node_ip():
    # Define the possible interface names in the desired order of preference
    #interface_names = ['eth0', 'eth1', 'enp3s0', 'enp4s0f0', 'eth2', 'eth3', 'ens0', 'ens1', 'p1p1', 'p2p1']
    interface_names = ['hv0', 'hv1', 'hv2', 'hv3', 'hv4', 'hv5', 'hv6', 'hv7', 'hv8', 'hv9']
    
    # Iterate over the interface names and select the first available IP address
    job_initiator_node_interface = None
    job_initiator_node_ip = None

    for interface_name in interface_names:
        if interface_name in ni.interfaces():
            try:
                # Get the IP address of the interface
                ip_address = ni.ifaddresses(interface_name)[ni.AF_INET][0]['addr']

                # If the IP address exists, set the job_initiator_node_interface and job_initiator_node_ip
                if ip_address:
                    job_initiator_node_interface = interface_name
                    job_initiator_node_ip = ip_address
                    break
            except KeyError:
                pass

    # If no IP address is found, fallback to the default IP address (127.0.0.1)
    if not job_initiator_node_interface or not job_initiator_node_ip:
        job_initiator_node_ip = socket.gethostbyname(socket.gethostname())
        job_initiator_node_interface = 'default'

    # Print the job_initiator node IP address
    print(f"Selected job_initiator node IP address: {job_initiator_node_ip} (Interface: {job_initiator_node_interface})")

    return job_initiator_node_ip, job_initiator_node_interface


# Get the IP and port of a job_initiator node (this could be one of your containers)
# job_initiator_node_ip = socket.gethostbyname(socket.getfqdn())  # Replace with the IP of your job_initiator node
# Call the function to retrieve the job_initiator node IP address
if os.getenv("WORKER_INDEX") == "1":
    job_initiator_node_ip, job_initiator_node_interface = get_job_initiator_node_ip()
    os.environ["JOB_INITIATOR_NODE_IP"] = job_initiator_node_ip
else:
    job_initiator_node_ip = os.getenv("JOB_INITIATOR_NODE_IP")

job_initiator_node_start_port = 12345  # Replace with the port of your job_initiator node for start synchronization
job_initiator_node_end_port = 12346  # Replace with the port of your job_initiator node for end synchronization
num_workers = 2  # Replace with the number of workers


def server(port, expected_msg, response_msg):
    # Set up a server socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((job_initiator_node_ip, port))  # use the specific port passed as argument
    s.listen(num_workers)

    # Keep track of the number of ready messages received
    num_received = 0

    # Store the client sockets to send them the response message later
    clientsockets = []

    while True:
        # Accept a connection
        clientsocket, address = s.accept()
        clientsockets.append(clientsocket)
        print(f"Connection from {address} has been established.")

        # Receive a message
        data = clientsocket.recv(1024)

        # If the message is the expected_msg, increment the count of received messages
        if data == expected_msg:
            num_received += 1
            print(f"Received {expected_msg.decode('utf-8')} message from {address}. Total {expected_msg.decode('utf-8')}: {num_received}")

        # If all containers are ready, send a response_msg message to each container and break the loop
        if num_received == num_workers:
            print(f"All workers are {expected_msg.decode('utf-8')}. Sending {response_msg.decode('utf-8')} message...")
            for cs in clientsockets:
                cs.send(response_msg)
            break

    # Close the server socket
    s.close()

# If this is the first worker, start the server in a separate thread for start and end synchronizations
if os.getenv("WORKER_INDEX") == "1":
    threading.Thread(target=server, args=(job_initiator_node_start_port, b"ready", b"start")).start()  # start synchronization
    threading.Thread(target=server, args=(job_initiator_node_end_port, b"done", b"end")).start()  # end synchronization

# Create a socket and connect to the job_initiator node
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect((job_initiator_node_ip, job_initiator_node_start_port))
        break
    except socket.error as e:
        #print(f"Failed to connect: {e.strerror}. Retrying in 1 second...")
        print(f"Failed to connect to job initiator: {e.strerror}. Retrying...")
        time.sleep(1)

if os.getenv("WORKER_INDEX") != "1":
    print(f"Container {os.getenv('WORKER_INDEX')} connected to job initiator. Launching job after full synchronization...")

# Send a message to the job_initiator node to indicate that this container is ready
s.sendall(b"ready")

# Receive a message from the job_initiator node. This will block until the job_initiator node sends a message.
data = s.recv(1024)

# Close the socket
s.close()

# Check the message. If it's "start", all containers are ready and this container can start.
if data != b"start":
    print(f"Received unexpected message: {data.decode('utf-8')}. Exiting...")
    exit(1)

# Print a message to indicate that the container has started
print(f"Container {os.getenv('WORKER_INDEX')} started.")

class SimpleCNN(pl.LightningModule):
    def __init__(self):
        super(SimpleCNN, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.drop_out = nn.Dropout()
        self.fc1 = nn.Linear(7 * 7 * 64, 1000)
        self.fc2 = nn.Linear(1000, 10)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        out = self.drop_out(out)
        out = self.fc1(out)
        out = self.fc2(out)
        return out

    def training_step(self, batch, batch_idx):
        images, labels = batch
        outputs = self(images)
        loss = nn.CrossEntropyLoss()(outputs, labels)
        self.log('train_loss', loss, on_step=False, on_epoch=True)
        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=0.001)

    def train_dataloader(self):
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])
        dataset = datasets.MNIST('.', train=True, download=True, transform=transform)
        return DataLoader(dataset, batch_size=64, shuffle=True)


# Instantiate the model
model = SimpleCNN()

trainer = Trainer(devices=1, accelerator="gpu", strategy="ddp", num_nodes=2, max_epochs=5)
trainer.fit(model)
print("Training complete.")

# Perform allreduce operation to synchronize model weights and biases across all workers
print("Waiting for remaining workers to finish...")

# Create a socket and connect to the job_initiator node
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect((job_initiator_node_ip, job_initiator_node_end_port))  # use port 12346 for end synchronization
        break
    except socket.error as e:
        #print(f"Failed to connect: {e.strerror}. Retrying in 1 second...")
        print(f"Failed to connect to job initiator: {e.strerror}. Retrying...")
        
        time.sleep(1)

# Send a message to the job_initiator node to indicate that this worker is done
s.sendall(b"done")

# Receive a message from the job_initiator node. This will block until the job_initiator node sends a message.
data = s.recv(1024)

# Close the socket
s.close()

# Check the message. If it's "end", all workers are done and this worker can end.
if data != b"end":
    print(f"Received unexpected message: {data.decode('utf-8')}. Exiting...")
    exit(1)

print("Model weights and biases synchronized across all workers.")

# Machine 1 with 5 NVIDIA GPUs and 1 AMD GPU

# docker run -ti --gpus device=0 --privileged --network=host -e WORKER_INDEX=1 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv0 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/23-optimize-single-gpu-allocation-per-container-and-enable-inter-container-communication-for/examples/tensorflow/horovod-tf-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container1 tf-nvidiagpu-horovod-hyprspace

# docker run -ti --gpus device=1 --privileged --network=host -e WORKER_INDEX=2 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv1 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/23-optimize-single-gpu-allocation-per-container-and-enable-inter-container-communication-for/examples/tensorflow/horovod-tf-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container2 tf-nvidiagpu-horovod-hyprspace

# docker run -ti --gpus device=2 --privileged --network=host -e WORKER_INDEX=3 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv2 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/23-optimize-single-gpu-allocation-per-container-and-enable-inter-container-communication-for/examples/tensorflow/horovod-tf-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container3 tf-nvidiagpu-horovod-hyprspace

# docker run -ti --gpus device=3 --privileged --network=host -e WORKER_INDEX=4 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv3 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/23-optimize-single-gpu-allocation-per-container-and-enable-inter-container-communication-for/examples/tensorflow/horovod-tf-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container4 tf-nvidiagpu-horovod-hyprspace

# docker run -ti --device=/dev/kfd --device=/dev/dri/card4 --device=/dev/dri/renderD132 --group-add video --privileged --network=host -e WORKER_INDEX=5 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv4 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/23-optimize-single-gpu-allocation-per-container-and-enable-inter-container-communication-for/examples/tensorflow/horovod-tf-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container5 tf-amdgpu-horovod-hyprspace

# Machine 2 with 1 NVIDIA GPU

# docker run -ti --gpus device=0 --privileged --network=host -e WORKER_INDEX=6 -e HOROVOD_MPI_THREADS_DISABLE=1 -e HOROVOD_TIMELINE=/app/horovod_timeline.json -e HV_CONFIG=hv5 -e ML_JOB="https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/23-optimize-single-gpu-allocation-per-container-and-enable-inter-container-communication-for/examples/tensorflow/horovod-tf-job-hyprspace-test.py" -e ML_JOB_DEPENDENCIES="" -v /tmp:/app --name container6 tf-nvidiagpu-horovod-hyprspace
