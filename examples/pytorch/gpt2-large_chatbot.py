import gradio as gr
import torch
from transformers import AutoTokenizer, AutoModelForCausalLM

# Load the pre-trained model and tokenizer
model_name = "gpt2-large"
tokenizer = AutoTokenizer.from_pretrained(model_name)
tokenizer.add_special_tokens({'pad_token': '[PAD]'})
model = AutoModelForCausalLM.from_pretrained(model_name).to("cuda")

# Define a function to generate a response to user input
def chatbot(text):
    # Tokenize the input text and convert to a PyTorch tensor
    input_ids = tokenizer.encode(text, return_tensors="pt", padding=True, truncation=True).to("cuda")
    # Generate a response using the model
    output = model.generate(input_ids, max_length=1000, pad_token_id=tokenizer.eos_token_id,
                            do_sample=True, top_p=0.9, temperature=1.0)
    # Decode the response and return it
    response = tokenizer.decode(output[0], skip_special_tokens=True)
    return response

# Create a Gradio interface
iface = gr.Interface(fn=chatbot, inputs="text", outputs="text", title="ChatBot",
                     description="Enter some text and the chatbot will generate a response.")
iface.launch(share=True)
