import torch
import gradio as gr
import os
from transformers import LlamaTokenizer, LlamaForCausalLM

# Specify the model path
model_path = 'openlm-research/open_llama_7b'

# Load the tokenizer and the model
tokenizer = LlamaTokenizer.from_pretrained(model_path)
model = LlamaForCausalLM.from_pretrained(model_path, torch_dtype=torch.float16)

# Use CUDA to leverage GPU
device = torch.device("cuda")
model = model.to(device)
filename = "conversation.txt"

if not os.path.exists(filename):

    open(filename, "w").close()  # create file if it doesn't exist

def generate_text(prompt: str) -> str:

    # Open the file in append mode
    with open(filename, "a+") as f:
        f.seek(0)  # go to the beginning of the file
        previous_conversation = f.read().splitlines()  # read previous conversation
        f.write(prompt + "\n")  # save user input
        # Convert the prompt to model inputs
        input_ids = tokenizer(prompt, return_tensors="pt").input_ids.to(device)
        # Generate text
        generation_output = model.generate(input_ids=input_ids, max_new_tokens=100)
        # Decode the output and return it
        output_text = tokenizer.decode(generation_output[0].cpu(), skip_special_tokens=True)
        f.write(output_text + "\n")  # save model response to file
        f.seek(0)  # go to the beginning of the file again
        updated_conversation = f.read().splitlines()  # read updated conversation

    return "\n".join(updated_conversation[len(previous_conversation):])

if __name__ == "__main__":

    chat_help_text = "Welcome! This ChatBot is designed to answer questions about a wide range of topics. " \
                     "Please note that the ChatBot may not always provide accurate or complete answers, and may not " \
                     "understand certain questions. Bear in mind that the ChatBot is not perfect and may provide " \
                     "inaccurate answers, so it's best suited for simple factual questions. The ChatBot can also remember conversations."

    iface = gr.Interface(fn=generate_text, inputs="text", outputs="text", title="Open_Llama_7b",
                         description=chat_help_text)

    iface.launch(share=True)
