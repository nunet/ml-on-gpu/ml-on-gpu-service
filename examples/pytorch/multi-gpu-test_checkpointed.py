import torch
import torch.nn as nn
import time

if torch.cuda.is_available():
    print("Number of GPUs available: ", torch.cuda.device_count())
    for i in range(torch.cuda.device_count()):
        print("Device ", i, ":", torch.cuda.get_device_name(i))
else:
    print("No GPU available")

# Define the model
class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.fc = nn.Linear(10, 10)
    
    def forward(self, x):
        return self.fc(x)

# Initialize the model and move it to GPU(s)
model = Model()
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
if torch.cuda.device_count() > 1:
    print("Using", torch.cuda.device_count(), "GPUs.")
    model = nn.DataParallel(model)
model = model.to(device)

# Define loss function and optimizer
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

# Dummy input for testing
#inputs = torch.randn(128, 10).to(device)
#labels = torch.randn(128, 10).to(device)
inputs = torch.randn(20, 10).to(device)
labels = torch.randn(20, 10).to(device)
#inputs = torch.randn(20, 128).to(device)
#labels = torch.randn(20, 128).to(device)

import glob

# Try to read the counter from a file
try:
    with open("epoch_count.txt", "r") as f:
        epoch_counter = int(f.read())
except FileNotFoundError:
    # If the file doesn't exist, create it with a default value of 0
    with open("epoch_count.txt", "w") as f:
        f.write("0")
    epoch_counter = 0


# Save the epoch count in a file
def save_epoch_count(epoch_counter):
    with open("epoch_count.txt", "w") as f:
        f.write(str(epoch_counter))


# Load the epoch count from a file
def load_epoch_count():
    with open("epoch_count.txt", "r") as f:
        epoch_counter = int(f.read())
    return epoch_counter

# Define the weights_init function
def weights_init(m):
    if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
        nn.init.xavier_uniform_(m.weight)
        nn.init.zeros_(m.bias)


# Load the model's weights from the last saved checkpoint
checkpoint_files = glob.glob("*.pth")

if checkpoint_files:
    # last_checkpoint = checkpoint_files[-1]
    last_checkpoint = checkpoint_files[-1]
    checkpoint = torch.load(last_checkpoint)
    model.load_state_dict(checkpoint["state_dict"])
else:
    # Initialize the model's weights
    last_checkpoint = "checkpoint.pth"
    model.apply(weights_init)

# Load the epoch count from a file
start_epoch = load_epoch_count() - 1

start_time = time.time()
num_epochs = 1000000 # Total number of epochs to run
log_interval = 10 # Interval for printing the loss

for epoch in range(start_epoch + 1, num_epochs):
    # Run a forward pass and calculate loss
    outputs = model(inputs)
    loss = criterion(outputs, labels)

    # Backpropagate the gradients and update the model's parameters
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    # Print the loss every `log_interval` epochs
    if epoch % log_interval == 0:
        print("Epoch:", epoch, "Loss:", loss.item(), "Elapsed time:", time.time() - start_time, "s")
        # Save the model's weights and the epoch count in a file
    state = {'state_dict': model.state_dict()}
    torch.save(state, last_checkpoint)
    save_epoch_count(epoch)

print("Training completed!")
