import os
import time
from pyamdgpuinfo import detect_gpus, get_gpu

# To increase threshold in floats
def float_range(start, stop, step):
    while start < stop:
        yield start
        start += step

# Function to sort GPU VRAM
def get_sorted_gpus(num_gpus):
    gpu_info = []
    for gpu_id in range(num_gpus):
        gpu = get_gpu(gpu_id)
        total_vram = gpu.memory_info["vram_size"]
        gpu_info.append((gpu_id, gpu, total_vram))
    return sorted(gpu_info, key=lambda x: x[2], reverse=True)

# Function to select GPU
def select_gpu():
    num_gpus = detect_gpus()
    sorted_gpus = get_sorted_gpus(num_gpus)
    # If there's only one GPU, select it at the lowest possible real-time usage between 0-30%
    if num_gpus == 1:
        print("Machine has a single AMD GPU. Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:        
            for load_threshold in float_range(0.0, 0.31, 0.05):  # increasing load threshold from 0% to 30%
                for memory_threshold in float_range(0.01, 0.31, 0.05):  # increasing memory threshold from 1% to 30%
                    gpu_id, gpu, _ = sorted_gpus[0]
                    vram_usage = gpu.query_vram_usage()
                    total_vram = gpu.memory_info["vram_size"]
                    memory_util = vram_usage / total_vram                
                    if gpu.query_load() <= load_threshold and memory_util <= memory_threshold:
                        os.environ["HIP_VISIBLE_DEVICES"] = str(gpu_id)
                        print(f"Selected Single GPU {gpu_id} ({gpu.name})...")
                        return
            time.sleep(5)  # wait for 5 seconds before checking again
            attempts += 1
        raise Exception("Single AMD GPU is using more than 30% of resources. Exiting...")

    # If there are multiple GPUs, start picking from the first one
    elif num_gpus > 1:
        print("Machine has multiple AMD GPUs. Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:
            for gpu_id, gpu, _ in sorted_gpus:
                vram_usage = gpu.query_vram_usage()
                total_vram = gpu.memory_info["vram_size"]
                memory_util = vram_usage / total_vram
                if gpu.query_load() == 0 and memory_util < 0.01:  # 0% utilization and <1% VRAM utilization
                    os.environ["HIP_VISIBLE_DEVICES"] = str(gpu_id)
                    print(f"Selected GPU {gpu_id} ({gpu.name})...")
                    return
            time.sleep(5)  # wait for 5 seconds before checking again
            attempts += 1
    raise Exception("Suitable AMD GPU(s) unavailable!")