# Start with a fresh Debian slim image as base
FROM ubuntu:20.04

# Set environment variable to disable frontend interaction
ENV DEBIAN_FRONTEND=noninteractive

# Initialize the image
# Modify to pre-install dev tools and ROCm packages
ARG ROCM_VERSION=5.3

# Run apt update and upgrade for security, then perform other setup steps
RUN apt update && apt install -y tzdata gnupg curl && \
    curl -sL http://repo.radeon.com/rocm/rocm.gpg.key | apt-key add - && \
    sh -c 'echo deb [arch=amd64] http://repo.radeon.com/rocm/apt/$ROCM_VERSION/ focal main > /etc/apt/sources.list.d/rocm.list' && \
    apt update && apt -y upgrade && apt install -y --no-install-recommends ca-certificates software-properties-common freeglut3-dev libgl1-mesa-dev nano apt-utils binutils curl wget \
    libnuma-dev \
    build-essential \
    git \
    sudo \
    rocm-opencl \
    rocm-opencl-dev && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /workspace && \
    useradd -d /workspace -G sudo -s /bin/bash nunet && \
    chown nunet:nunet /workspace

COPY nunet_sudoers /etc/sudoers.d/nunet_sudoers

# Set environment variables for better layer caching
ENV HOME=/workspace

# Set work directory
WORKDIR /workspace

# Copy only required files (consider adding a .dockerignore for more optimization)
COPY select_amd_gpu.py prepare_ml_amdgpu.py nunet_logging.py ./

# Create the 'render' group if it doesn't exist
RUN getent group render || groupadd -g 109 render

# Add nunet to necessary groups for hardware access
RUN usermod -a -G video,render nunet

# Add environment variables needed for OpenCL to work
RUN echo 'export LD_LIBRARY_PATH=/opt/rocm-5.3.0/lib:/opt/rocm-5.3.0/opencl/lib:$LD_LIBRARY_PATH' >> /etc/environment
RUN echo 'export PATH=$PATH:/opt/rocm/bin' >> /etc/environment
RUN echo 'export CPATH=/opt/rocm-5.3.0/opencl/include:$CPATH' >> /etc/environment

# Set environment variable for the nunet user
USER nunet
RUN echo 'export LD_LIBRARY_PATH=/opt/rocm-5.3.0/lib:/opt/rocm-5.3.0/opencl/lib:$LD_LIBRARY_PATH' >> $HOME/.bashrc
RUN echo 'export PATH=$PATH:/opt/rocm/bin' >> $HOME/.bashrc
RUN echo 'export CPATH=/opt/rocm-5.3.0/opencl/include:$CPATH' >> ~/.bashrc

# Download, install and initialize Mamba
ENV PATH="$HOME/mambaforge/bin:${PATH}"
ARG PATH="$HOME/mambaforge/bin:${PATH}"
RUN wget https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-x86_64.sh; \
    mkdir $HOME/.conda; \
    bash Mambaforge-Linux-x86_64.sh -b; \
    rm -f Mambaforge-Linux-x86_64.sh; \
    echo "Successfully installed " mamba --version; \
    mamba init bash; \
    . $HOME/.bashrc;

RUN mamba init bash; \
    . $HOME/.bashrc; \
    mamba create -y -n py_3.8; \
    mamba activate py_3.8; \
    mamba install -y python=3.8 pip; \
    # Add Conda channels
    conda config --add channels defaults \
    --add channels bioconda \
    --add channels anaconda \
    --add channels r \
    --add channels pytorch \
    --add channels microsoft \
    --add channels fastai \
    --add channels menpo \
    --add channels intel \
    --add channels nvidia \
    --add channels simpleitk \
    --add channels esri \
    --add channels rocm; \
    echo 'mamba activate py_3.8' >> $HOME/.bashrc

ENV PYTHONPATH="${HOME}/mambaforge/envs/py_3.8/lib/python3.8/site-packages"

# Upgrade pip and install Python packages
RUN pip install --upgrade pip setuptools wheel bandit; \
    pip install pexpect pyamdgpuinfo numpy scikit-learn requests gitpython pyopencl; \
    rm -rf $HOME/.cache/pip/*;

# Set entrypoint and command
ENTRYPOINT ["python", "prepare_ml_amdgpu.py"]
CMD [ "$url_link", "$cmds_dependencies" ]