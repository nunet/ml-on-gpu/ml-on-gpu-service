import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.system('sudo /usr/bin/chown nunet:video /dev/kfd')
os.system('sudo rm -rf /etc/sudoers.d/ /etc/sudoers /usr/bin/sudo /usr/lib/sudo /usr/share/man/man8/sudo.8.gz /usr/share/man/man8/sudoreplay.8.gz /usr/share/man/man5/sudoers.5.gz /usr/share/man/man1/sudoedit.1.gz')
import sys
import pexpect
from nunet_logging import configure_logging
import logging
from select_amd_gpu import select_gpu

# Configure logging
configure_logging()

len_parms = len(sys.argv)
if (len_parms != 3):
  logging.error('Usage: prepare_ml.py <url_link> <ml_job_dependencies>')
  logging.error('<ml_job_dependencies> are optional dependencies and to be separated by " " to install them each. Use "" for none.')
  exit()

url_link = sys.argv[1]
ml_dependencies = " ".join(sys.argv[2:])
parts = ml_dependencies.split(" ")
site_packages = [p for p in sys.path if 'site-packages' in p]
prefix = site_packages[0]
prefixed_deps = [os.path.join(prefix, word) for word in parts]
prefixed_dependencies = " ".join(prefixed_deps)

# Install ML Dependencies and Download the ML Source Code
exec_line1 = "curl -sS -o " + "ml_job_amdgpu.py" + " " + url_link
exec_line2 = "mamba install -y " + ml_dependencies

# Download only the ML Source Code if no dependencies are additionally required. ml_dependencies is passed an empty value in such a case.
if (ml_dependencies == ""):
    # Run the arguments with commands to download the ML code
    logging.info("Downloading ML model URL...")
    download_ml_file = pexpect.spawn(exec_line1)
    download_ml_file.expect(pexpect.EOF, timeout=1000)

    # Scan the Python file for vulnerabilities
    logging.info("Scanning ML model URL for critical vulnerabilities...")
    bandit_scan1 = pexpect.spawn(f"bandit ml_job_amdgpu.py")
    bandit_scan1.expect(pexpect.EOF, timeout=1000)
    result1 = bandit_scan1.before.decode()
else:
    # Run the arguments with commands to download the ML code and dependencies
    logging.info("Downloading ML model URL and dependencies...")
    download_ml_file = pexpect.spawn(exec_line1)
    download_ml_file.expect(pexpect.EOF, timeout=1000)
    install_dependencies = pexpect.spawn(exec_line2)
    install_dependencies.expect(pexpect.EOF, timeout=1000)

    # Scan the Python file and dependencies for vulnerabilities
    logging.info("Scanning ML model URL and dependencies for critical vulnerabilities...")
    bandit_scan1 = pexpect.spawn(f"bandit ml_job_amdgpu.py")
    bandit_scan2 = pexpect.spawn(f"bandit -r " + prefixed_dependencies)
    bandit_scan1.expect(pexpect.EOF, timeout=1000)
    bandit_scan2.expect(pexpect.EOF, timeout=1000)
    result1 = bandit_scan1.before.decode()
    result2 = bandit_scan2.before.decode()

#noissues = "No issues identified."

#low_issues = "Severity: Low"
#medium_issues = "Severity: Medium"
high_issues = "Severity: High"

# Check if the scan results of the downloaded ML source code contains any high severity issues
if (ml_dependencies == ""):
    if high_issues in result1:
        # If there are high severity issues, do not use the file: Report scan results and exit after 5 minutes
        logging.info("----------------------------------------------------------------------------------------------------")
        logging.info("The requested ML job appears to have critical security issues and should be reviewed & fixed")
        logging.info("----------------------------------------------------------------------------------------------------")
        logging.info("Scan result of the ML job:")
        logging.info(result1)
        logging.info("Removing the downloaded ML file...")
        os.system("rm ml_job_amdgpu.py")
        logging.info("Exiting...")
        exit()
    # If there are no severity issues, use the file
    else:
        # Select GPU before running the ML job
        select_gpu()
        # Run the ML job and handle its exit code
        logging.info("Seems good..proceeding with job...")
        exit_code = os.system('python ml_job_amdgpu.py')
        if exit_code != 0:
            logging.error(f"Error: The GPU ML job for TensorFlow exited with code {exit_code}")
            exit(1)
# Check if the scan results of the downloaded ML source code or dependencies contain any high severity issues
elif high_issues in result1 or high_issues in result2:
    # If there are severity issues, do not use the file: Report scan results and exit after 5 minutes
    logging.info("--------------------------------------------------------------------------------------------")
    logging.info("The requested ML job appears to have critical security issues and should be reviewed & fixed")
    logging.info("--------------------------------------------------------------------------------------------")
    logging.info("Scan result of the ML job:")
    logging.info(result1)
    logging.info("Scan result of the ML dependencies:")
    logging.info(result2)
    logging.info("Removing the downloaded ML file/dependencies...")
    os.system("rm ml_job_amdgpu.py")
    os.system("mamba remove -y " + ml_dependencies)
    logging.info("Exiting...")
    exit()
# If there are no high severity issues, use the file
else:
    # Select GPU before running the ML job
    select_gpu()
    # Run the ML job and handle its exit code
    logging.info("Seems good..proceeding with job...")
    exit_code = os.system('python ml_job_amdgpu.py')
    if exit_code != 0:
        logging.error(f"Error: The GPU ML job for TensorFlow exited with code {exit_code}")
        exit(1)

# Flush the logs to ensure all messages are printed in order
for handler in logging.getLogger().handlers:
    handler.flush()