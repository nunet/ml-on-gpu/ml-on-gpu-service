# Use a minimal base image
FROM rocm/pytorch:latest

# Run apt update and upgrade for security, then perform other setup steps
RUN apt update && \
    apt -y upgrade && \
    apt install -y --no-install-recommends apt-utils binutils curl && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /workspace && \
    useradd -d /workspace -s /bin/bash nunet && \
    chown nunet:nunet /workspace

# Upgrade pip and install Python packages
RUN pip install --upgrade pip setuptools wheel bandit && \
    pip install pexpect pyamdgpuinfo && \
    rm -rf /root/.cache/pip/*

# Set environment variables for better layer caching
ENV HOME=/workspace \
    PATH="/workspace/.local/bin:$PATH"

# Set work directory
WORKDIR /workspace

# Copy only required files (consider adding a .dockerignore for more optimization)
COPY select_amd_gpu.py prepare_ml_amdgpu.py nunet_logging.py check-rocm-and-hip-availability.py ./

# Switch to user "nunet"
USER nunet

# Set entrypoint and command
ENTRYPOINT ["python", "prepare_ml_amdgpu.py"]
CMD [ "$url_link", "$cmds_dependencies" ]