import torch

GOOD_C='\033[32m'   # GREEN
BAD_C='\033[31m'    # RED
NORMAL_C='\033[0m'

if torch.cuda.is_available():

  print("{}{}{}".format(GOOD_C, "CUDA is available\n", NORMAL_C))
  if torch.backends.cudnn.is_acceptable(torch.tensor([0], dtype=torch.float32, device="cuda")):
    print("{}{}{}".format(GOOD_C, "Tensor Cores are available\n", NORMAL_C))
  else:
    print("{}{}{}".format(BAD_C, "Tensor Cores are not available\n", NORMAL_C))
else:
  print("{}{}{}".format(BAD_C, "CUDA is not available\n", NORMAL_C))
