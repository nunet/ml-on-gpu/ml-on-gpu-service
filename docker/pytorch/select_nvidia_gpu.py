import os
import time
import torch
from GPUtil import getGPUs

# To increase threshold in floats
def float_range(start, stop, step):
    while start < stop:
        yield start
        start += step

# Function to select GPU
def select_gpu():
    # Get GPU properties from PyTorch
    torch_gpus = [torch.cuda.get_device_properties(i) for i in range(torch.cuda.device_count())]
    # Create a mapping of GPU names to PyTorch IDs
    torch_gpu_id_mapping = {gpu.name: i for i, gpu in enumerate(torch_gpus)}

    # Sort GPUs by VRAM using GPUtil
    GPUs = sorted(getGPUs(), key=lambda gpu: gpu.memoryTotal, reverse=True)
    # If there's only one GPU, select it at the lowest possible real-time usage between 0-30%
    if len(GPUs) == 1:
        # Single GPU logic
        gpu = GPUs[0]
        print(f"Machine has a single NVIDIA GPU ({gpu.name}). Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:
            for load_threshold in float_range(0.0, 0.31, 0.05):  # increasing load threshold from 0% to 30%
                for memory_threshold in float_range(0.01, 0.31, 0.05):  # increasing memory threshold from 1% to 30%
                    if gpu.load <= load_threshold and gpu.memoryUtil <= memory_threshold:
                        pytorch_id = torch_gpu_id_mapping.get(gpu.name)
                        if pytorch_id is not None:
                            print(f"Selected Single GPU {pytorch_id} ({gpu.name})...")
                            return str(pytorch_id)
                        else:
                            print(f"Warning: GPU {gpu.name} not found in PyTorch enumeration.")
            time.sleep(5)  # wait for 5 seconds before checking again
            attempts += 1
        raise Exception("Single NVIDIA GPU is using more than 30% of resources. Exiting...")

    # If there are multiple GPUs, start picking from the first one
    elif len(GPUs) > 1:
        print("Machine has multiple NVIDIA GPUs. Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:
            for gpu in GPUs:
                if gpu.load == 0.0 and gpu.memoryUtil < 0.01:  # 0% utilization and <1% VRAM usage
                    pytorch_id = torch_gpu_id_mapping.get(gpu.name)
                    if pytorch_id is not None:
                        print(f"Selected GPU {pytorch_id} ({gpu.name})...")
                        return str(pytorch_id)
                    else:
                        print(f"Warning: GPU {gpu.name} not found in PyTorch enumeration.")
            time.sleep(5)  # wait for 5 seconds before checking again
            attempts += 1
    raise Exception("Suitable NVIDIA GPU(s) unavailable!")