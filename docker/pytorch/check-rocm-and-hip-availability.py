import torch

GOOD_C='\033[32m'   # GREEN
BAD_C='\033[31m'    # RED
NORMAL_C='\033[0m'

if torch.cuda.is_available():

  print("{}{}{}".format(GOOD_C, "ROCm is available\n", NORMAL_C))

  if hasattr(torch.version, 'hip') and torch.version.hip:
    print("{}{}{}".format(GOOD_C, "HIP is available\n", NORMAL_C))
  else:
    print("{}{}{}".format(BAD_C, "HIP is not available\n", NORMAL_C))
else:
  print("{}{}{}".format(BAD_C, "ROCm is not available\n", NORMAL_C))
