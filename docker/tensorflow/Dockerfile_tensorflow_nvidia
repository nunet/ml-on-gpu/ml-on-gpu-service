# Use a minimal base image
FROM tensorflow/tensorflow:latest-gpu

# Run apt update and upgrade for security, then perform other setup steps
RUN apt update && \
    apt -y upgrade && \
    apt install -y --no-install-recommends apt-utils && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    sed -i 's/^ldconfig/# ldconfig/g' /etc/bash.bashrc && \
    mkdir -p /workspace && \
    useradd -d /workspace -s /bin/bash nunet && \
    chown nunet:nunet /workspace

# Upgrade pip and install Python packages
RUN pip install --upgrade pip setuptools wheel bandit && \
    pip install pexpect GPUtil && \
    rm -rf /root/.cache/pip/*

# Set environment variables for better layer caching
ENV HOME=/workspace \
    PATH="/workspace/.local/bin:$PATH"

# Set work directory
WORKDIR /workspace

# Copy only required files (consider adding a .dockerignore for more optimization)
COPY select_nvidia_gpu.py prepare_ml.py nunet_logging.py ./

# Switch to user "nunet"
USER nunet

# Set entrypoint and command
ENTRYPOINT ["python", "prepare_ml.py"]
CMD [ "$url_link", "$cmds_dependencies" ]