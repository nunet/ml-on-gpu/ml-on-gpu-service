import os
import time
import tensorflow as tf
from GPUtil import getGPUs

# To increase threshold in floats
def float_range(start, stop, step):
    while start < stop:
        yield start
        start += step

def create_gpu_mapping():
    # TensorFlow GPU enumeration and names
    tf_gpus = tf.config.list_physical_devices('GPU')
    tf_gpu_names = [tf.config.experimental.get_device_details(gpu)['device_name'] for gpu in tf_gpus]

    # GPUtil GPU enumeration and names
    gputil_gpus = getGPUs()
    gputil_gpu_names = [gpu.name for gpu in gputil_gpus]

    # Create mapping from GPUtil ID to TensorFlow ID based on matching names
    mapping = {}
    for tf_id, tf_name in enumerate(tf_gpu_names):
        for gputil_id, gputil_name in enumerate(gputil_gpu_names):
            if tf_name == gputil_name:
                mapping[gputil_id] = tf_id

    return mapping

# Function to select GPU
def select_gpu():
    # Create GPU ID mapping
    gpu_id_mapping = create_gpu_mapping()

    # Sort GPUs by VRAM using GPUtil
    GPUs = sorted(getGPUs(), key=lambda gpu: gpu.memoryTotal, reverse=True)

    if len(GPUs) == 1:
        # Single GPU logic
        gpu = GPUs[0]
        print(f"Machine has a single NVIDIA GPU. Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:
            for load_threshold in float_range(0.0, 0.31, 0.05):  # increasing load threshold from 0% to 30%
                for memory_threshold in float_range(0.01, 0.31, 0.05):  # increasing memory threshold from 1% to 30%
                    if gpu.load <= load_threshold and gpu.memoryUtil <= memory_threshold:
                        tf_gpu_id = gpu_id_mapping.get(gpu.id, gpu.id)
                        print(f"Selected Single GPU {tf_gpu_id} ({gpu.name})...")
                        return str(tf_gpu_id)
            time.sleep(5)
            attempts += 1
        raise Exception("Single NVIDIA GPU is using more than 30% of resources. Exiting...")

    # If there are multiple GPUs, start picking from the first one
    elif len(GPUs) > 1:
        # Multiple GPUs logic
        print("Machine has multiple NVIDIA GPUs. Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:
            for gpu in GPUs:
                if gpu.load == 0.0 and gpu.memoryUtil < 0.01:  # 0% utilization and <1% VRAM usage
                    tf_gpu_id = gpu_id_mapping.get(gpu.id, gpu.id)
                    print(f"Selected GPU {tf_gpu_id} ({gpu.name})...")
                    return str(tf_gpu_id)
            time.sleep(5)
            attempts += 1
    raise Exception("Suitable NVIDIA GPU(s) unavailable!")