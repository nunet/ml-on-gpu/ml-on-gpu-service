import os
import time
from GPUtil import getGPUs

# To increase threshold in floats
def float_range(start, stop, step):
    while start < stop:
        yield start
        start += step

# Function to select GPU
def select_gpu():
    GPUs = getGPUs()
    # If there's only one GPU, select it at the lowest possible real-time usage between 0-30%
    if len(GPUs) == 1:
        print("Machine has a single NVIDIA GPU. Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:
            for load_threshold in float_range(0.0, 0.31, 0.05):  # increasing load threshold from 0% to 30%
                for memory_threshold in float_range(0.01, 0.31, 0.05):  # increasing memory threshold from 1% to 30%
                    gpu = GPUs[0]
                    if gpu.load <= load_threshold and gpu.memoryUtil <= memory_threshold:
                        os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu.id)
                        print(f"Selected Single GPU {str(gpu.id)} ({gpu.name})...")
                        return
            time.sleep(5)  # wait for 5 seconds before checking again
            attempts += 1
        raise Exception("Single NVIDIA GPU is using more than 30% of resources. Exiting...")

    # If there are multiple GPUs, start picking from the first one
    elif len(GPUs) > 1:
        print("Machine has multiple NVIDIA GPUs. Waiting for an optimal GPU state...")
        max_attempts = 20
        attempts = 0
        while attempts < max_attempts:
            for gpu in GPUs:
                if gpu.load == 0.0 and gpu.memoryUtil < 0.01:  # 0% utilization and <1% VRAM usage
                    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu.id)
                    print(f"Selected GPU {str(gpu.id)} ({gpu.name})...")
                    return
                print(f"GPU {str(gpu.id)} ({gpu.name}) is currently in use. Attempting to select the next NVIDIA GPU...")
            time.sleep(5)  # wait for 5 seconds before checking again
            attempts += 1
    raise Exception("Suitable NVIDIA GPU(s) unavailable!")
    
