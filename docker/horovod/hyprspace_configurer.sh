#!/bin/bash

# Get the number of workers as an input
num_workers=$1

# Check if the number of workers is provided
if [ -z "$num_workers" ]
then
    echo "Please provide the number of workers as an argument."
    exit 1
fi

# Generate Hyprspace configuration files for each worker
for ((i=0; i<$num_workers; i++))
do
    # Generate a new Hyprspace configuration file
    sudo hyprspace init hv$i
    # Manually set the IP address and listening port
    sed -i "s/address: .*/address: 10.99.99.$((i+1))\/24/" /etc/hyprspace/hv$i.yaml
    sed -i "s/listen_port: .*/listen_port: $((8001+i))/" /etc/hyprspace/hv$i.yaml
done

# Create an array to store peer IDs
declare -a peer_ids

# Store the peer IDs in the array
for ((i=0; i<$num_workers; i++))
do
    peer_ids[i]=$(awk '/id:/ {print $2}' /etc/hyprspace/hv$i.yaml | tr -d '\n')
done

# Update the configuration files to include all other workers as peers
for ((i=0; i<$num_workers; i++))
do
    for ((j=0; j<$num_workers; j++))
    do
        if [ $i -ne $j ]
        then
            # Get the IP address of the other worker
            ip_address=$(awk '/address:/ {print $2}' /etc/hyprspace/hv$j.yaml | cut -d'/' -f1)

            # Add the other worker as a peer
            awk -v ip="$ip_address" -v id="${peer_ids[j]}" '/peers:/ {print $0; print "  " ip ":"; print "    id: " id; next}1' /etc/hyprspace/hv$i.yaml > temp && mv temp /etc/hyprspace/hv$i.yaml
        fi
    done
    # Remove the empty curly braces
    sed -i 's/peers: {}/peers:/g' /etc/hyprspace/hv$i.yaml
done
