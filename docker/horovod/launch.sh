#!/bin/sh
FILEPATH=/etc/hyprspace/
CONFIG=$HV_CONFIG
echo "Filepath: $FILEPATH"
echo "Config: $CONFIG"

if [ -f "$FILEPATH$CONFIG.yaml" ]; then
    echo "***********************  Config file contents:  ***********************"
    cat $FILEPATH$CONFIG.yaml
    echo "*************** Launching container into Hyprspace ********************"
    sudo hyprspace up $CONFIG
else 
    sudo hyprspace init $CONFIG
fi

    #Entrypoint for the container to prepare and run the ML Job on the onboarder's machine
    echo "Job: $ML_JOB"
    echo "Worker Index: $WORKER_INDEX"
    echo "HOROVOD_MPI_THREADS_DISABLE: $HOROVOD_MPI_THREADS_DISABLE"
    echo "HOROVOD_TIMELINE: $HOROVOD_TIMELINE"


if [ -f ./"prepare_ml.py" ]; then
    python prepare_ml.py "$ML_JOB" "$ML_JOB_DEPENDENCIES"
else
    python prepare_ml_amdgpu.py "$ML_JOB" "$ML_JOB_DEPENDENCIES"
fi
