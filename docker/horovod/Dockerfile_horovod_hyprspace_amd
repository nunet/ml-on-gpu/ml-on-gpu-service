# Using the TensorFlow GPU image as base
FROM rocm/tensorflow:latest

# Upgrade the image to include the latest security updates and patches
RUN apt update && apt install -y apt-utils && apt -y upgrade && \
# Upgrade pip inside the container
    pip install --upgrade pip setuptools wheel bandit && \
# Install pexpect for using interactive input and pyamdgpuinfo for selecting GPU
    pip install pexpect pyamdgpuinfo && \
# Install netifaces to fetch job initiator IP
    pip install netifaces && \
# Install PyTorch and other machine learning libraries
    pip install --no-cache-dir \
    torch -f https://download.pytorch.org/whl/rocm5.3/torch_stable.html \
    torchvision && \
# Install Lightning framework (including PyTorch Lightning)
    pip install lightning

# Install Horovod with TensorFlow and PyTorch support
ENV HOROVOD_GPU=ROCM \
    HOROVOD_GPU_OPERATIONS=NCCL \
    HOROVOD_WITH_TENSORFLOW=1 \
    HOROVOD_WITH_PYTORCH=1

#    HOROVOD_WITH_MXNET=1
RUN pip install horovod[ray] && \
# Create /workspace directory
    mkdir /workspace && \
# Add user "nunet" with home directory set to /workspace and login shell set to /bin/bash
    useradd -d /workspace -s /bin/bash nunet && \
    apt install -y sudo && \
    usermod -aG sudo nunet && \
# Change ownership of /workspace to user "nunet"
    chown nunet:nunet /workspace

# Set home directory as /workspace
ENV HOME=/workspace \
# Add dependencies directory to PATH
    PATH="/workspace/.local/bin:$PATH"

# Set /workspace as work directory
WORKDIR /workspace

# AMD GPU selection library
COPY prepare_ml_amdgpu.py select_amd_gpu.py nunet_logging.py hyprspace_configurer.sh launch.sh ./

# url_link: URL where the model will be downloaded
# cmds_dependencies: list of dependencies to be installed by pip
CMD [ $url_link, $cmds_dependencies ]

# Create directory for hyprspace
RUN mkdir -p /usr/local/bin/ && chown -R nunet:nunet /usr/local/bin/ && \
# Download hyprspace
    curl -L -o /usr/local/bin/hyprspace https://github.com/hyprspace/hyprspace/releases/download/v0.2.2/hyprspace-v0.2.2-linux-amd64 && \
# Install hyprspace
    chmod a+x /usr/local/bin/hyprspace && \
    ln -s /usr/local/bin/hyprspace /usr/bin/hyprspace && \
# Create config folder for hyprspace
    mkdir -p /etc/hyprspace && chown -R nunet:nunet /etc/hyprspace && \
    echo "nunet ALL=(ALL) NOPASSWD: /usr/local/bin/hyprspace" > /etc/sudoers.d/hyprspace && \
    chmod 0440 /etc/sudoers.d/hyprspace && \
# Copy Hyprspace Configurer
    chmod a+x hyprspace_configurer.sh && \
# Install some troubleshooting tools
    apt update && apt install iputils-ping nano -y && \
# Copy launch.sh script and define default command for the container
    chmod a+x launch.sh

# Create /app directory and set permissions
RUN mkdir -p /app && chown nunet:nunet /app && \
# Create the Horovod timeline file and set its ownership to nunet
    touch /app/horovod_timeline.json && chown nunet:nunet /app/horovod_timeline.json

# Switch to user "nunet"
USER nunet

# Set the entrypoint as the non-root user
ENTRYPOINT ["./launch.sh"]